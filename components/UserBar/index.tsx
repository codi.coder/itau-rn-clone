import React from 'react';
import Icon from 'react-native-vector-icons/Feather';

import {
  HeaderWrapper,
  UserInfoContainer,
  UserAvatar,
  UserAccountInfo,
  WelcomeMessage,
  AccountInfo,
} from './styles';

const UserBar: React.FC = () => {
  return (
    <HeaderWrapper>
      <UserInfoContainer>
        <UserAvatar
          source={{
            uri: 'https://api.adorable.io/avatars/100/joao.png',
          }}
          style={{width: 50, height: 50}}
        />
        <UserAccountInfo>
          <WelcomeMessage>Olá, Joao</WelcomeMessage>
          <AccountInfo>Ag ..00 c/c ...00-0</AccountInfo>
        </UserAccountInfo>
      </UserInfoContainer>
      <Icon name="chevron-down" size={42} color="#ff895e" />
    </HeaderWrapper>
  );
};

export default UserBar;
