import styled from 'styled-components/native';

export const HeaderWrapper = styled.View`
  background-color: #fff;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 20px 10px;
`;

export const UserInfoContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const UserAvatar = styled.Image`
  border-radius: 25px;
  margin-right: 20px;
`;

export const UserAccountInfo = styled.View``;

export const WelcomeMessage = styled.Text`
  color: #ff895e;
  font-weight: 600;
`;

export const AccountInfo = styled.Text`
  color: #ff895e;
  font-weight: 600;
`;
