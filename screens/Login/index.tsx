import React from 'react';
import {Text} from 'react-native';
import UserBar from '../../components/UserBar';
import {LoginBackground} from './styles';

const Login: React.FC = () => {
  return (
    <LoginBackground>
      <UserBar />

      <Text>Hello World</Text>
    </LoginBackground>
  );
};

export default Login;
