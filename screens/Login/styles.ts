import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components';

export const LoginBackground = styled(LinearGradient).attrs({
  colors: ['#ff895e', '#ff8f66'],
})`
  flex: 1;
`;
